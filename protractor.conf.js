exports.config = {
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    getPageTimeout: 60000,
    allScriptsTimeout: 500000,
    framework: 'custom',
    // path relative to the current config file
    frameworkPath: require.resolve('protractor-cucumber-framework'),
    capabilities: {
        'browserName': 'chrome'
    },

    // Spec patterns are relative to this directory.
    specs: [
        'features/*/*.feature'
    ],

    baseURL: 'http://localhost:8080/',

    cucumberOpts: {
        require: 'features/step_definitions/step_definitions.js',
        tags: false,

        format: ['pretty', 'json:reports/cucumber.json'],
        profile: false,

        'no-source': true
    },

    beforeLaunch: function() {
        var BehavePro = require('behavepro');
        return new Promise(function(resolve) {
            BehavePro({
                "id": 10100,
                "userId": "ZjRiM2I0YjUtZGI1MC0zYjhlLWE2NTEtM2NiZWNmYzllMDll",
                "apiKey": "1d6c2cd5012a286b92bc0a49005ec8b08b71c72e"
            }, function() {
                resolve();
            });
        });
    }
};
