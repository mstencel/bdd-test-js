module.exports = function () {

    var chai = require('chai');
    var chaiAsPromised = require('chai-as-promised');

    chai.use(chaiAsPromised);
    var expect = chai.expect;

    this.Given(/^I go to "([^"]*)"$/, function (site, callback) {
        browser.get(site).then(callback);
    });

    this.When(/^I add "([^"]*)" in the task field$/, function (taskName, callback) {
        element(by.model("todoList.todoText")).sendKeys(taskName);
        callback();
    });

    this.When(/^I click the add button$/, function (callback) {
        element(by.css('[value="add"]')).click();
        callback();
    });

    this.Then(/^I should see my new task "([^"]*)" in the list$/, function (taskName, callback) {
        var todoList = element.all(by.repeater('todo in todoList.todos'));
        expect(todoList.count()).to.eventually.equal(3);
        expect(todoList.get(2).getText()).to.eventually.equal(taskName).and.notify(callback);
    });

    this.When(/^I type "([^"]*)" in the name field$/, function (name, callback) {
        element(by.model("yourName")).sendKeys(name);
        callback();
    });

    this.Then(/^I should see "([^"]*)"$/, function (helloText, callback) {
        var hello = element(by.css('[app-run="hello.html"]')).element(by.tagName("h1"));
        expect(hello.getText()).to.eventually.equal(helloText).and.notify(callback);
    });
}
